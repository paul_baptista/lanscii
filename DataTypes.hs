-----------------------------------------------------------------------------
-- |
-- Modulo       :   DataTypes
--
-- Autores      :   Daniel Leones (09-10977) <mailto:metaleones@gmail.com>,
--                  Paul Baptista (10-10056) <mailto:paul.baptista@gmail.com>
--
-- Grupo        :   16
--
-- Licencia     :   Apache License 2.0
--
-- Modulo DataTypes. Posee las declaraciones de los tipos necesarios en los
-- módulos Lexer y Parser
-- 
-- Universidad Simón Bolívar
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------
module DataTypes where


--Datatypes y Types utilizados en el lexer

{-|
  Tipo de datos @Canvas@. Representa los diferentes tipos de lienzos presentes
  en el lenguaje.

  Se hace uso de esta estructura de datos auxiliar para definir las instacias
  de @Show@ y @Read@ necesarias en este caso

  Para ser utilizado en el Lexer.
-}
data Canvas = Canvas String deriving(Eq)

{-|
  Instancia de @Read@ para el tipo @Canvas@
-}
instance Read Canvas where
  readsPrec _ s = readsCanvas s
    where
      readsCanvas ('#':ls) = [(Canvas "#", ls)]     
      readsCanvas ('<':c:'>':ls) = [(Canvas ('<':c:'>':[]) , ls)]

{-|
  Instancia de @Show@ para el tipo @Canvas@
-}
instance Show Canvas where
  show (Canvas (_:s:[_])) = '"':s:'"':[]
  show (Canvas "#") = "\"#\""

{-|
  Tipo de datos @BinBool@. Representa las operaciones de conjunción y
  disyunción de booleanos.

  Se hace uso de esta estructura de datos auxiliar para definir las instacias
  de @Show@ y @Read@ necesarias en este caso.

  Para ser utilizado en el Lexer.
-}
data BinBool = BinBool String deriving(Eq)

{-|
  Instancia de @Read@ para el tipo @BinBool@
-}
instance Read BinBool where
  readsPrec _ s = readsBinBool s
    where
      readsBinBool ('/':'\\':ls) = [(BinBool "/\\", ls)]     
      readsBinBool ('\\':'/':ls) = [(BinBool "\\/", ls)]

{-|
  Instancia de @Show@ para el tipo @BinBool@
-}
instance Show BinBool where
  show (BinBool str) = ('\"':) $ (++"\"") str




--Datatypes y Types utilizados en el parser 

{-|
  TypeAlias de @String@ para representar los identificadores en el parser
-}
type Identificador = String


{-|
  Tipo @LanscciiTipo@, representa los posibles tipos de datos de lanscii
-}
data LanscciiTipo
  = TipoEntero      -- ^ Representación del tipo correspondiente a los enteros
  | TipoBool        -- ^ Representación del tipo correspondiente a los booleanos
  | TipoCanvas      -- ^ Representación del tipo correspondiente a los lienzos
  deriving(Eq,Show,Ord)


{-|
  Tipo de datos que representa el conjunto de instrucciones existentes en 
  Lanscii. 
-}
data Stmt
  = Alcance     [(LanscciiTipo,[String])] [Stmt]
    -- ^ Representa un bloque de 
    --   código, con sus declaraciones e instrucciones correspondientes.

  | ReadIns     Identificador -- ^ Representa a la instrucción @read@, con su
                              --   parámetro correspondiente
  | WriteIns    CanvExpr      -- ^ Representa a la instrucción @write@, con su
                              --   parámetro correspondiente

  | Asignacion  Identificador Expr
                              -- ^ Representa a la asignación, con su
                              --   identificador y expreción adecuados.

  | Condicional BoolExpr [Stmt] [Stmt] 
                              -- ^ Representa a la instrucción condicional,
                              -- con su condición y conjuntos de instrucciones
                              --   correspondientes

  | IterIndet   BoolExpr [Stmt]
                              -- ^ Representa a la instrucción de iteración 
                              --   de iteración indeterminada, con su
                              --   condición de ciclo y conjunto de
                              --   instrucciones

  | IterDet     AritExpr AritExpr [Stmt]
                              -- ^ Representa a la instrucción de iteración 
                              --   determinada sin identificador específico
                              --   asociado.

  | IterDet'    Identificador AritExpr AritExpr [Stmt] 
                              -- ^ Representa a la instrucción de iteración
                              --   determinada con identificador específico
                              --   asociado.
  deriving(Eq)

{-|
  Instancia de @Show@ para el tipo @Stmt@ 
-}
instance Show Stmt where
  show (Alcance l1 l2) =
    let
      declaraciones =
        if null l1 then ""
        else
          "Declaraciones:\n" ++
          concatMap
            ( (++"\n") .
              (\(t,vs) -> "  " ++ show t ++ ": " ++ concatMap (++" ") vs) )
            l1

      instrucciones =
        if null l2 then ""
        else
          "Instrucciones:\n" ++
              init (concatMap
                      ((concatMap (("|  "++).(++"\n"))) . lines . show ) l2)
    in
      declaraciones ++ instrucciones

  show (ReadIns ident) =
    "Read:" ++ "\n" ++ 
    "|  " ++ "Identificador:\n" ++ 
    "|  |  " ++ show ident

  show (WriteIns expr) =
    "Write:" ++ "\n" ++
    "|  " ++ "Expresión:" ++ "\n" ++ 
           init (concatMap (("|  |  "++).(++"\n")) (lines $ show expr))

  show (Asignacion ident expr) =
    "Asignacion:" ++ "\n"  ++
    "|  " ++ "Identificador:" ++ "\n" ++
    "|  |  " ++ ident ++ "\n" ++
    "|  " ++ "Expresión:" ++ "\n" ++ 
           init (concatMap (("|  |  "++).(++"\n")) (lines $ show expr))

  show (Condicional bexpr st st') =
    let
      lexpr =
        concatMap (("|  |  "++).(++"\n")) (lines $ show bexpr)
      lst =
        concatMap
          ((concatMap (("|  |  "++).(++"\n"))) . lines . show ) st
      lst' =
        concatMap
          ((concatMap (("|  |  "++).(++"\n"))) . lines . show ) st'
    in
      "Condicional:" ++ "\n" ++
      "|  " ++ "Condición:" ++  "\n" ++ lexpr ++
      "|  " ++ "Then:" ++ "\n" ++ init lst ++
      ( if null lst' then ""
        else
          "\n" ++
          "|  " ++ "Else:" ++ "\n" ++ init lst'
      )

  show (IterIndet bexpr st) =
    "Iteración Determinada:" ++ "\n" ++ 
    "|  " ++ "Condición:" ++ "\n" ++ 
           concatMap (("|  |  "++).(++"\n")) (lines $ show bexpr) ++
    "|  " ++ "Cuerpo:" ++ "\n" ++
           init (concatMap
                  (concatMap (("|  |  "++).(++"\n")) . lines . show ) st)

  show (IterDet e1 e2 st) =
      "Iteración Indeterminada:" ++ "\n" ++
      "|  " ++ "Límite inferior:" ++ "\n" ++ 
             concatMap (("|  |  "++).(++"\n")) (lines $ show e1) ++
      "|  " ++ "Límite superior:" ++ "\n" ++ 
             concatMap (("|  |  "++).(++"\n")) (lines $ show e2) ++ 
      "|  " ++ "Cuerpo:" ++ "\n" ++
             init (concatMap
                    (concatMap (("|  |  "++).(++"\n")) . lines . show ) st)

  show (IterDet' ident e1 e2 st) =
      "Iteración Indeterminada:" ++ "\n" ++
      "|  " ++ "Contador:" ++ "\n" ++
      "|  |  " ++ show ident ++ "\n" ++
      "|  " ++ "Límite inferior:" ++ "\n" ++
             concatMap (("|  |  "++).(++"\n")) (lines $ show e1) ++
      "|  " ++ "Límite superior:" ++ "\n" ++
             concatMap (("|  |  "++).(++"\n")) (lines $ show e2) ++
      "|  " ++ "Cuerpo:\n" ++
            init (concatMap
                  (concatMap (("|  |  "++).(++"\n")) . lines . show ) st)


{-|
  Tipo de datos @Expr@. Utilizado para representar los diferentes tipos de
  exprexión posibles en Lanscii.
-}
data Expr
  = ExpresionAritmetica AritExpr      
          -- ^ Representación genérica de una expresión aritmética
  | ExpresionBooleana   BoolExpr      
          -- ^ Representación genérica de una expresión lógica
  | ExpresionCanvas     CanvExpr      
          -- ^ Representación genérica de una expresión de lienzos
  | Bracket             Expr          
          -- ^ Representación genérica de una expresión parentizada
  | Variable            Identificador
          -- ^ Representación de un identificador usado como expresión expresión
  deriving(Eq)

{-|
  Instancia de Show para el tipo Expr
-}
instance Show Expr where
  show (ExpresionAritmetica e) =
    --"Expresión\naritmética:" ++ "\n" ++
      --init (concatMap (("|  "++).(++"\n")) (lines $ show e))
      show e
  show (ExpresionBooleana e) =
    --"Expresión\nbooleana:" ++ "\n" ++
      --init (concatMap (("|  "++).(++"\n")) (lines $ show e))
      show e
  show (ExpresionCanvas e) =
    --"Expresión\nde canvas:" ++ "\n" ++
      --init (concatMap (("|  "++).(++"\n")) (lines $ show e))
      show e
  show (Bracket e) =
    "Paréntesis:" ++ "\n" ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e))
  show (Variable id) =
    --"Variable:" ++ "\n" ++ "|  " ++ id
    "Variable: " ++ id

tipoExpr :: Expr -> Maybe LanscciiTipo
tipoExpr expr = case expr of
  ExpresionAritmetica _ -> Just TipoEntero
  ExpresionBooleana   _ -> Just TipoBool
  ExpresionCanvas     _ -> Just TipoCanvas
  Bracket          expr -> tipoExpr expr
  Variable            _ -> Nothing

{-|
  Tipo @AritExpr@. Utilizado para representar las expresiones aritméticas.
-}
data AritExpr
  = EnteroLiteral   Int       -- ^ Representación de un entero literal

  | MenosUnario     Expr      -- ^ Representación del menos unario de una
                              --   expresión

  | Suma            Expr Expr -- ^ Representación de la suma aritmética entre
                              --   dos expresiones
  | Resta           Expr Expr -- ^ Representación de la resta aritmética entre
                              --   dos expresiones
  | Multiplicacion  Expr Expr -- ^ Representación de la multiplicación 
                              --   aritmética entre dos expresiones
  | Division        Expr Expr -- ^ Representación de la división aritmética entre
                              --   dos expresiones
  | Modulo          Expr Expr -- ^ Representación del menos unario aritmético
                              --   entre dos expresiones 
  deriving(Eq)

{-|
  Instancia de @Show@ para el tipo @AritExpr@
-}
instance Show AritExpr where
  show (EnteroLiteral x) = "Entero: " ++ show x

  show (MenosUnario x) =
    "Menos\nunario:" ++ "\n" ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show x))

  show (Suma x y) =
    "Suma:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (Resta x y) =
    "Resta:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (Multiplicacion x y) =
    "Multiplicación:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (Division x y) =
    "División:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (Modulo x y) =
    "Módulo:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))


{-|
  Tipo @BoolExpr@. Utilizado para representar las expresiones booleanas.
-}
data BoolExpr
  = BooleanoLiteral   String    -- ^ Representación literal de un booleano

  | Negacion          Expr      -- ^ Representación de la negación lógica de una
                                --   expresión
  | Conjuncion        Expr Expr -- ^ Representación de la conjunción lógica
                                --   entre dos expresiones
  | Disyuncion        Expr Expr -- ^ Representación de la disyunción lógica 
                                --   entre dos expresiones

  | OpRelacion        Relacion  -- ^ Representación como booleano de una
                                --   relación entre dos expresiones
  deriving(Eq)

{-|
  Instancia de @Show@ para el tipo BoolExpr
-}
instance Show BoolExpr where  
  show (BooleanoLiteral str) = "Booleano: " ++ str
  
  show (OpRelacion r) = show r

  show (Negacion e) =
    "Negación:" ++ "\n" ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e))

  show (Conjuncion x y) = 
    "Conjunción:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (Disyuncion x y) = 
    "Disyunción:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))


{-|
  Tipo @Relación@. Usado para representar las expresiones relacionales.
-}
data Relacion
  = MayorQue      Expr Expr -- ^ Representación de la relación @>@ entre dos
                            --   expresiones
  | MayorIgualQue Expr Expr -- ^ Representación de la relación @>=@ entre dos
                            --   expresiones
  | MenorQue      Expr Expr -- ^ Representación de la relación @<@ entre dos
                            --   expresiones
  | MenorIgualQue Expr Expr -- ^ Representación de la relación @<=@ entre dos
                            --   expresiones

  | Igual         Expr Expr -- ^ Representación de la relación de igualdad @=@
                            --   entre dos expresiones
  | NoIgual       Expr Expr -- ^ Representación de la relación de no igualdad
                            --   @/=@ entre dos expresiones
  deriving(Eq)

{-|
  Instancia de @Show@ para el tipo @Relacion@
-}
instance Show Relacion where
  show (MayorQue e1 e2) =
    "> :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

  show (MenorQue e1 e2) =
    "< :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

  show (MayorIgualQue e1 e2) =
    "≥ :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

  show (MenorIgualQue e1 e2) =
    "≤ :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

  show (Igual e1 e2) =
    "= :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

  show (NoIgual e1 e2) =
    "≠ :" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show e1) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show e2))

{-|
  Tipo @CanvExpr@. Utilizado para representar las expresiones de lienzos
-}
data CanvExpr =
    CanvaLiteral      Canvas    -- ^ Representación de un lienzo litaral como
                                --   expresión.

  | Rotacion          Expr      -- ^ Representación de la rotación de un lienzo
                                --   como expresión
  | Transposicion     Expr      -- ^ Representación de la transposición de un 
                                --   lienzo como expresión

  | ConcatVertical    Expr Expr -- ^ Representación como expresión de la
                                --   concatenación vertical entre dos lienzos
  | ConcatHorizontal  Expr Expr -- ^ Representación como expresión de la 
                                --   concatenación horizontal entre dos lienzos 

  deriving(Eq)

{-|
  Instancia de @Show@ para el tipo @CanvExpr@
-}
instance Show CanvExpr where
  show (CanvaLiteral cv) = "Canva: " ++ show cv

  show (Rotacion cv) =
    "Rotación:" ++ "\n" ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show cv))

  show (Transposicion cv) =
    "Transposición:" ++ "\n" ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show cv))

  show (ConcatVertical x y) =
    "Concatenación\nVertical:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))

  show (ConcatHorizontal x y) =
    "Concatenación\nHorizontal:" ++ "\n" ++
      concatMap (("|  "++).(++"\n")) (lines $ show x) ++
      init (concatMap (("|  "++).(++"\n")) (lines $ show y))
