DOCDIR   = doc
BINARIES = Lanscii
OBJECTS  = *.o *.hi
SOURCES  = Lanscii.hs
LEXER 	 = Lexer.hs
PARSER   = Parser.hs
MODULES  = $(LEXER) $(PARSER)

all:	$(BINARIES) doc

Lanscii: $(SOURCES) DataTypes.hs $(MODULES)
	ghc --make Lanscii

$(LEXER): Lexer.x
	alex Lexer.x

$(PARSER): Parser.y
	happy Parser.y -iinfoParser.txt

clean:
	rm -rf Lex*.hs Par*.hs $(OBJECTS) $(BINARIES) doc infoParser.txt

doc:	$(SOURCES) $(MODULES)
	test -d $(DOCDIR) || mkdir $(DOCDIR)
	rm -rf $(DOCDIR)/*
	haddock --html --odir=$(DOCDIR) $(SOURCES)