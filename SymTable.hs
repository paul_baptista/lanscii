module SymbolTable 
  ( SymbolTable
  , create
  , insert
  , delete
  , update
  , elem
  , lookup
  , main
  , readAndprint
  )
where

import Parser
import Lexer
import DataTypes
import Data.Maybe
import Prelude hiding (elem,lookup)
import qualified Scope as Scope

import System.Environment
import System.IO

data SymbolTable
  = SymbolTable Scope.Scope [SymbolTable] (Maybe SymbolTable) deriving(Eq)

instance Show SymbolTable where
  show (SymbolTable sc hj pr) =
    concatMap (("|  "++) . (++"\n")) . lines $
      (Scope.printSc sc) ++
      ( concatMap (('\n':).show) $ filter ((/=sc).getScope) hj )

getScope (SymbolTable sc _ _) = sc


create :: Stmt -> Maybe SymbolTable -> SymbolTable
create stmt st =
  let
    miScope = Scope.create stmt
    sinHijos = SymbolTable miScope [] st
  in 
    case stmt of
      Alcance _ stmts ->
        SymbolTable miScope (map ((flip create) (Just sinHijos)) stmts) st

      IterDet' ident _ _ stmts ->
        SymbolTable miScope (map ((flip create) (Just sinHijos)) stmts) st

      otherwise ->
        maybe (error "WTF!") (id) st


insert :: Identificador -> (LanscciiTipo, Maybe Expr) -> SymbolTable -> SymbolTable
insert ident val (SymbolTable sc hj pr) =
  updateTree $ SymbolTable (Scope.insert ident val sc) hj pr

updateTree (SymbolTable sc hj pr) =
  let
    sinHijos = SymbolTable sc [] pr
  in
    SymbolTable sc (map ( updateTree . (updateParent sinHijos)) hj) pr  

updateParent (SymbolTable sc hj pr) newparent =
  SymbolTable sc hj (Just newparent)


delete :: Identificador -> SymbolTable -> SymbolTable
delete ident (SymbolTable sc hj pr) =
    updateTree $ SymbolTable (Scope.delete ident sc) hj pr


update :: Identificador -> (LanscciiTipo, Maybe Expr) -> SymbolTable -> SymbolTable
update ident val (SymbolTable sc hj pr) =
    updateTree $ SymbolTable (Scope.update ident val sc) hj pr


elem :: Identificador -> SymbolTable -> Bool
elem ident (SymbolTable sc _ pr) =
  let
    inScope = Scope.elem ident sc
  in
    if inScope then
      True
    else
      maybe (False) (SymbolTable.elem ident) pr


lookup :: Identificador -> SymbolTable -> Maybe (LanscciiTipo, Maybe Expr)
lookup ident (SymbolTable sc _ pr) =
  let
    inScope = Scope.lookup ident sc
  in
    maybe (maybe (Nothing) (SymbolTable.lookup ident) pr) (Just) inScope


main :: IO ()
main =
  do
    args <- getArgs
    fileName <- getFilename args
    hSetBuffering stdout NoBuffering
    readAndprint fileName

readAndprint fileName =
  readFile fileName >>=
      print . (flip create) Nothing . parser . lexer

getFilename :: [String] -> IO String
getFilename args =
  if (null args) then
    do
      hSetBuffering stdout NoBuffering
      putStr "Archivo a Interpretar: "
      fileName <- getLine
      return fileName
  else
    do
      return (head args)