{
{-|
  Modulo       :   Lexer
 
  Autores      :   Daniel Leones (09-10977) <mailto:metaleones@gmail.com>,
                   Paul Baptista (10-10056) <mailto:paul.baptista@gmail.com>
 
  Grupo        :   16
 
  Licencia     :   Apache License 2.0
 
  Modulo desarrollado en Alex. Implementa un analizador lexicográfico para el
  lenguaje *Lanscii*, siguiendo la especificación del mismo.
  
  Universidad Simón Bolívar
  Caracas - Venezuela
 
  Trimestre Abril-Julio 2015
-}


module Lexer
  (
    -- * Tipos exportados
    -- ** /Tokens/ retornados por el analizador lexicográfico. 
    Token(..)

    -- * Funciones exportadas
    -- ** Analizador lexicográfico
  , lexer   -- :: String -> [Token]

  ) where

import DataTypes
  ( Canvas(..)
  , BinBool(..)
  )

}

%wrapper "posn"

$digito = 0-9           -- UN digito
$letra  = [a-zA-Z]      -- UNA letra

tokens :-
  $white+                             ;

  \{                                { \p s -> LeftCurly           s  (lyc p) }
  \|                                { \p s -> Pipe                s  (lyc p) }
  \}                                { \p s -> RightCurly          s  (lyc p) }
  \;                                { \p s -> Semicolon           s  (lyc p) }

  read                              { \p s -> Read                s  (lyc p) }
  write                             { \p s -> Write               s  (lyc p) }

  \[                                { \p s -> LeftBracket         s  (lyc p) }
  \]                                { \p s -> RightBracket       s  (lyc p) }
  \:                                { \p s -> Colon               s  (lyc p) }
  \.\.                              { \p s -> TwoDots             s  (lyc p) }
  \?                                { \p s -> QuetionMark         s  (lyc p) }

  \!                                { \p s -> ExclamationMark     s  (lyc p) }
  $digito+                          { \p s -> LiteralNumber (read s) (lyc p) }
  \+                                { \p s -> Plus                s  (lyc p) }
  \-                                { \p s -> Minus               s  (lyc p) }
  \*                                { \p s -> Asterisk            s  (lyc p) }
  \/                                { \p s -> Slash               s  (lyc p) }

  \%                                { \p s -> Percent             s  (lyc p) }
  true                              { \p s -> TRUE                s  (lyc p) }
  false                             { \p s -> FALSE               s  (lyc p) }
  \/\\                              { \p s -> Conjunction   (read s) (lyc p) }
  \\\/                              { \p s -> Disjunction   (read s) (lyc p) }
  \=                                { \p s -> EqualSym            s  (lyc p) }
  \/\=                              { \p s -> NotEqualSym         s  (lyc p) }
  \^                                { \p s -> OPNot               s  (lyc p) }

  \<                                { \p s -> OPLessThan          s  (lyc p) }
  \<\=                              { \p s -> OPLessOrEqThan      s  (lyc p) }
  \>                                { \p s -> OPGreaterThan       s  (lyc p) }
  \>\=                              { \p s -> OPGreaterOrEqThan   s  (lyc p) }

  @                                 { \p s -> AtSym               s  (lyc p) }
  (\<[\/ \\ _ \- \| \  ]\> | \#)    { \p s -> CANVAS        (read s) (lyc p) }
  \'                                { \p s -> SingleQuote         s  (lyc p) }
  \~                                { \p s -> Tilde               s  (lyc p) }
  &                                 { \p s -> Ampersand           s  (lyc p) }
  \$                                { \p s -> DollarSym           s  (lyc p) }

  \(                                { \p s -> LeftParentheses     s  (lyc p) }
  \)                                { \p s -> RightParentheses    s  (lyc p) }

  $letra [ $letra $digito _ ]*      { \p s -> Identifier          s  (lyc p) }

  \{\-(([^\-\}]|[^\-]\}|\-[^\}]|$white)*|\-$white*|\}$white*)\-\}  ;

  .                                 { reportError }
  

{

{-|
  El tipo de datos @Token@ representa los diferentes /tokens/ producidos por
  el Analizador Lexicográfico. Cada /token/ va acompañado de una tupla de
  enteros, cuyos componentes denotan la línea y columna, respectivamente, en 
  la cual se encontró el /token/ dentro del archivo procesado. Para aquellos
  /tokens/ que lo ameriten, se agrega el lexema convertido al tipo deseado.
-}
data Token
  = LeftCurly         String  (Int,Int)
  | Pipe              String  (Int,Int)
  | RightCurly        String  (Int,Int)
  | Semicolon         String  (Int,Int)

  | Read              String  (Int,Int)
  | Write             String  (Int,Int)

  | LeftBracket       String  (Int,Int)
  | RightBracket     String  (Int,Int)
  | Colon             String  (Int,Int)
  | TwoDots           String  (Int,Int)
  | QuetionMark       String  (Int,Int)

  | ExclamationMark   String  (Int,Int)
  | LiteralNumber     Int     (Int,Int)
  | Plus              String  (Int,Int)
  | Minus             String  (Int,Int)
  | Asterisk          String  (Int,Int)
  | Slash             String  (Int,Int)

  | Percent           String  (Int,Int)
  | TRUE              String  (Int,Int)
  | FALSE             String  (Int,Int)
  | Conjunction       BinBool (Int,Int)
  | Disjunction       BinBool (Int,Int)
  | EqualSym          String  (Int,Int)
  | NotEqualSym       String  (Int,Int)
  | OPNot             String  (Int,Int)

  | OPLessThan        String  (Int,Int)
  | OPLessOrEqThan    String  (Int,Int)
  | OPGreaterThan     String  (Int,Int)
  | OPGreaterOrEqThan String  (Int,Int)

  | RightParentheses  String  (Int,Int)
  | LeftParentheses   String  (Int,Int)

  | AtSym             String  (Int,Int)
  | CANVAS            Canvas  (Int,Int)  
  | SingleQuote       String  (Int,Int)
  | Tilde             String  (Int,Int)
  | Ampersand         String  (Int,Int)
  | DollarSym         String  (Int,Int)
  
  | Identifier        String  (Int,Int)

  deriving (Eq, Show)


{-|
  La función @lexer@ encapsula el uso del Analizador Lexicográfico.
  Recibe como único argumento un @String@, presumiblemente leído
  desde un archivo pero también es posible pasarlo explícitamente,
  y produce una lista de /tokens/ a medida que los va procesando.
-}
lexer :: String      -- ^ Cadena de caracteres @S@ a "tokenizar"
         -> [Token]  -- ^ Lista resultante de /tokens/ del tipo @Token@.

lexer s = alexScanTokens s

{-|
  Cada token debe ir acompañado de la línea y columna en la cual
  fue encontrado. El wrapper 'posn' genera para cada token el
  desplazamiento absoluto dentro del archivo, la linea y la columna.
  Como el enunciado del proyecto establece que solamente interesa
  la línea y la columna, la función lyc ("linea y columna") extrae
  solamente la línea y la columna, produciendo una tupla susceptible
  de ser incorporada directamente al token.
 -}
lyc :: AlexPosn -> (Int,Int)
lyc (AlexPn _ l c) = (l,c)


{-|
  Cuando ninguna de las expresiones regulares para palabras reservadas,
  símbolos, números o identificadores logra coincidir con la entrada,
  quiere decir que se ha encontrado un caracter inválido para MiniLogo.
  La última expresión regular de la lista captura ese caracter inválido
  y debe emitir un mensaje de error "razonable".

  Cuando se usa el wrapper 'posn' todas las acciones son invocadas
  pasando como parámetros tanto la posición generada por Alex como
  la cadena (en este caso de exactamente un caracter) que coincidió
  con la expresión regular. La función reportError se encarga de emitir
  un mensaje de error apropiado aprovechando esos parámetros.
-}
reportError :: AlexPosn -> String -> a
reportError p s = error m
  where
    m = "\nError (Lexer): Caracter inesperado '" ++ s ++
        "' en la linea " ++ (show l) ++ " y columna " ++ (show c) ++ "."
    (l,c) = lyc p

}
