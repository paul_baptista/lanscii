module Scope where

import DataTypes
import Data.Maybe
import qualified Data.HashMap.Lazy as Map


type Scope =
  ( Stmt
  , Map.HashMap Identificador (LanscciiTipo, Maybe Expr)
  )

printSc :: Scope -> String
printSc (stmt, mapa)
  | Map.null mapa = []
  | otherwise =
      case stmt of
        Alcance _ _      -> printsome
        IterDet' _ _ _ _ -> printsome
        _                -> []
      where
        printsome =
          Map.foldrWithKey
            (\x y -> (++) $ show x ++ ": " ++ show y ++ "\n") [] mapa


create :: Stmt -> Scope
create stmt = case stmt of
  Alcance decls stmts
    ->( Alcance decls stmts, 
      	( Map.fromList $ concatMap 
          ( \(tipo,ids) -> map (\y -> (y, (tipo,Nothing))) ids) decls 
        ) 
      )
  IterDet' id expr expr1 stmts
    -> ( stmt, (Map.singleton id (TipoEntero, Nothing)))

  _ -> (stmt, Map.empty)


insert :: Identificador -> (LanscciiTipo, Maybe Expr) -> Scope -> Scope
insert ident (typ,expr) (st,mp) =
  if Map.member ident mp then
    error $ "Variable " ++ ident ++ " redefinida."
  else
    (st, Map.insert ident (typ,expr) mp)


delete :: Identificador -> Scope -> Scope
delete ident (st,mp) = (st, Map.delete ident mp)


update :: Identificador -> (LanscciiTipo, Maybe Expr) -> Scope -> Scope
update ident (typ, expr) (st,mp) =
  if isNothing $ Map.lookup ident mp then
    error $ "Variable " ++ ident ++ " no definida."
  else
    (st, Map.adjust (\_ -> (typ, expr)) ident mp) 


elem :: Identificador -> Scope -> Bool
elem ident (st,mp) = Map.member ident mp


lookup :: Identificador -> Scope -> Maybe (LanscciiTipo, Maybe Expr)
lookup ident (st, mp) = Map.lookup ident mp