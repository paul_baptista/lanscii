{
-----------------------------------------------------------------------------
-- |
-- Modulo       :   Parser
--
-- Autores      :   Daniel Leones (09-10977) <mailto:metaleones@gmail.com>,
--                  Paul Baptista (10-10056) <mailto:paul.baptista@gmail.com>
--
-- Grupo        :   16
--
-- Licencia     :   Apache License 2.0
--
-- Modulo desarrollado en Happy. Implementa un analizador sintáctico para el
-- lenguaje *Lanscii*, siguiendo la especificación del mismo.
-- 
-- Universidad Simón Bolívar
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------

module Parser(parser) where

import System.Environment
import System.IO
import Lexer
import DataTypes
}

%name parser
%error { parserError }
%tokentype { Token }
%token
  '{'          { LeftCurly         _ _  }
  '|'          { Pipe              _ _  }
  '}'          { RightCurly        _ _  }
  ';'          { Semicolon         _ _  }

  read         { Read              _ _  }
  write        { Write             _ _  }

  '['          { LeftBracket       _ _  }
  ']'          { RightBracket     _ _  }
  ':'          { Colon             _ _  }
  ".."         { TwoDots           _ _  }
  '?'          { QuetionMark       _ _  }

  '!'          { ExclamationMark   _ _  }
  num          { LiteralNumber     $$ _ }
  '+'          { Plus              _ _  }
  '-'          { Minus             _ _  }
  '*'          { Asterisk          _ _  }
  '/'          { Slash             _ _  }

  '%'          { Percent           _ _  }
  true         { TRUE              $$ _ }
  false        { FALSE             $$ _ }
  '∧'          { Conjunction       _ _  }
  '∨'          { Disjunction       _ _  }
  '='          { EqualSym          _ _  }
  '≠'          { NotEqualSym       _ _  }
  '^'          { OPNot             _ _  }

  '<'          { OPLessThan        _ _  }
  '≤'          { OPLessOrEqThan    _ _  }
  '>'          { OPGreaterThan     _ _  }
  '≥'          { OPGreaterOrEqThan _ _  }

  '@'          { AtSym             _ _  }
  canva        { CANVAS            $$ _ }
  '~'          { Tilde             _ _  }
  '&'          { Ampersand         _ _  }
  '$'          { DollarSym         _ _  }
  sq           { SingleQuote       _ _  }

  '('          { LeftParentheses   _ _  }
  ')'          { RightParentheses  _ _  }

  id           { Identifier        $$ _ }


%nonassoc '=' '≠'

%left '∨'
%left '∧'
%right '^'

%nonassoc '<' '≤' '>' '≥'

%left '+' '-'
%left '*' '/' '%'
%left NEG

%left '&' '~'
%right '$'
%left sq

%%

Programa        : Alcance                 { $1 }

Alcance         : '{' Instrucciones '}'   { Alcance [] (reverse $2) }
                | '{'
                      Declaraciones
                    '|'
                      Instrucciones
                  '}'                     { Alcance (reverse $2) (reverse $4) }


Declaraciones   : Declaraciones Tipo Identificadores { ($2,$3):$1 }
                | Tipo Identificadores               { ($1,$2):[] }

Identificadores : Identificadores id                 { $2:$1 }
                | id                                 { [$1] }

Tipo            : '@'                                { TipoCanvas }
                | '%'                                { TipoEntero }
                | '!'                                { TipoBool }


Instrucciones   : Instrucciones ';' Instruccion      { $3:$1 }
                | Instruccion                        { [$1] }

Instruccion     : read id                            { ReadIns $2 }
                | write CanvExpr                     { WriteIns $2 }
                | Asignacion                         { $1 }
                | Condicional                        { $1 }
                | Iteracion                          { $1 }
                | Alcance                            { $1 }


Asignacion      : id '=' Expr                        { Asignacion $1 $3 }

Condicional     : '(' BoolExpr '?' Instrucciones ')' { Condicional $2 $4 [] }
                | '(' BoolExpr '?' Instrucciones
                      ':' Instrucciones
                  ')'                                { Condicional $2 $4 $6 }

Iteracion       : '[' BoolExpr '|' Instrucciones ']' { IterIndet $2 $4 }
                | '[' AritExpr ".." AritExpr
                    '|'
                      Instrucciones
                  ']'                                { IterDet $2 $4 $6}
                | '[' id ':' AritExpr ".." AritExpr
                    '|'
                      Instrucciones
                  ']'                                { IterDet' $2 $4 $6 $8}


Expr            : AritExpr            { ExpresionAritmetica $1 }
                | BoolExpr            { ExpresionBooleana   $1 }
                | CanvExpr            { ExpresionCanvas     $1 }
                | '(' Expr ')'        { Bracket  $2 }
                | id                  { Variable $1 }

AritExpr        : num                 { EnteroLiteral $1 }
                | '-' Expr %prec NEG  { MenosUnario $2 }
                | Expr '+' Expr       { Suma $1 $3 }
                | Expr '-' Expr       { Resta $1 $3 }
                | Expr '/' Expr       { Division $1 $3 }
                | Expr '%' Expr       { Modulo $1 $3 }
                | Expr '*' Expr       { Multiplicacion $1 $3 }

BoolExpr        : true                { BooleanoLiteral $1}
                | false               { BooleanoLiteral $1}
                | RelacExpr           { OpRelacion $1 }
                | Expr '^'            { Negacion $1}
                | Expr '∧' Expr       { Conjuncion $1 $3 }
                | Expr '∨' Expr       { Disyuncion $1 $3 }

RelacExpr       : Expr '>' Expr       { MayorQue $1 $3 }
                | Expr '≥' Expr       { MayorIgualQue $1 $3 }
                | Expr '<' Expr       { MenorQue $1 $3 }
                | Expr '≤' Expr       { MenorIgualQue $1 $3 }

                | Expr '=' Expr       { Igual $1 $3 }
                | Expr '≠' Expr       { NoIgual $1 $3 }

CanvExpr        : canva               { CanvaLiteral $1 }
                | Expr '&' Expr       { ConcatVertical   $1 $3 }
                | Expr '~' Expr       { ConcatHorizontal $1 $3 }
                | '$' Expr            { Rotacion $2 }
                | Expr sq             { Transposicion $1 }


{

{-|
  Función que ha de ejecutarse al encontrar un error sintáctico. 
  Imprime en stderr el token y ubicación correspondientes al error conseguido.
-}
parserError :: [Token] -> a
parserError (t:ts) = error $
                       "Error de sintaxis en el Token: " ++ (show t) ++ "\n" ++
                       "Seguido de: " ++ (unlines $ map show $ take 4 ts)

}

