{-|
   /Programa Principal - MiniLogo/


	 Traductores e Interpretadores CI3725 - Grupo 0

	 Versi�n 0.1 2007-05-01


   Grupo 0

	 86-17791 Ernesto Hern�ndez-Novich <mailto:emhn@usb.ve>


	 Programa Principal para el interpretador de MiniLogo.
   Se implantaron algunas funciones auxiliares cuya documentaci�n
	 est� en el texto del programa. Este programa incluye la
	 funcionalidad extra solicitada en el enunciado de la Primera
	 Parte del Proyecto Global.

   Una vez compilado, el programa ejecutable puede utilizarse de
	 dos formas diferentes:

	 * Si se invoca desde la l�nea de comandos /sin/ suministrar
	   argumentos adicionales, aparecer� un /prompt/ que solicita
		 al usuario el nombre del archivo a procesar.

	 * Si se invoca desde la l�nea de comandos /con/ argumentos
     adicionales, se asume que el /primer/ argumento corresponde
		 al nombre del archivo a procesar; el resto de los argumentos
		 suministrados ser�n ignorados.

	 El nombre del archivo puede ser relativo al directorio actual
	 o absoluto comenzando desde la ra�z del sistema de archivos.

   Si lo desea, puede utilizar este programa principal para sustituir
	 el suyo propio, con la condici�n que no puede cambiarlo salvo
	 cuando el Autor lo indique.

 -}
module Main (
	-- * Funci�n Principal.
  main
) where

import System
import System.IO
import Lexer

{-|
   Funci�n principal.

   El programa puede ser compilado para su ejecuci�n directa,
   o bien cargado en el interpretador GHCi e invocado a trav�s
   de la funci�n @main@.
 -}
main :: IO ()
main =
	do
		args <- getArgs
		fileName <- getFilename args
		contents <- readFile fileName
		print $ lexer contents

{-
   getFilename

	 Funci�n auxiliar para obtener el nombre del archivo a procesar.
	 Presenta un prompt en pantalla para que el usuario introduzca
	 el nombre del archivo que desea procesar y espera a que se
	 suministre una l�nea de texto. Esa l�nea de texto es retornada.

   La funci�n 'args' es provista por la librer�a System y permite
	 tener acceso a la lista de argumentos suministrados en la l�nea
	 de comandos, si �sta existe.
-}
getFilename args =	if null(args) then
											do 
												hSetBuffering stdout NoBuffering
												putStr "Archivo a Interpretar: "
												fileName <- getLine
												return fileName
										else
											do
												return (head args)
