{-|
    /M�quina Logo/


    Traductores e Interpretadores CI3725

    Versi�n 0.42 2007-06-18


    Ernesto Hern�ndez-Novich


    Este m�dulo implanta una m�quina interpretadora de instrucciones
    b�sicas Logo, a ser utilizada como /backend/ para la Tercera
    Parte del Proyecto.

    Este m�dulo depende de la Haskell Graphics Library (HGL) para
    operar correctamente. Ha sido probado satisfactoriamente sobre
    Debian GNU/Linux Etch 4.0 con GHCi y HGL tal y como vienen
    dispuestos en la distribuci�n, plataforma sobre la cual se
    realizar� la correcci�n.
-}
module LogoMachine (
  -- * Tipos exportados.
  -- ** Instrucciones de bajo nivel de la M�quina Logo.
  LogoProgram (..),
  -- * Funciones exportadas.
  -- ** Ejecutar instrucciones de la M�quina Logo con salida gr�fica.
  runLogoProgram
) 
where

import Data.Char
import Graphics.HGL

{-|
    El tipo de datos @LogoProgram@ representa las instrucciones
    "de m�quina" que es capaz de interpretar la M�quina Logo. La
    M�quina Logo dispone de una tortuga que puede avanzar o retroceder
    un n�mero entero de "pasos" sobre un plano cartesiano, girando
    a la derecha o izquierda un n�mero entero de grados. La tortuga
    dispone de l�pices de colores los cuales pueden bajarse o subirse
    para dibujar o no a medida que se avanza, cambiando el color de los
    l�pices si se desea.  La tortuga puede emitir un mensaje de texto
    arbitrario usando el color del l�piz actual, el cual aparecer�
    horizontalmente a la izquierda de la posici�n actual,
    independientemente de la orientaci�n.
    
    N�tese que adem�s de las instrucciones at�micas fundamentales,
    tambi�n se dispone de la instrucci�n de Secuenciaci�n y la
    instrucci�n de Interaci�n Determinada. Esto puede o no facilitar
    el desarrollo de su interpretador, dependiendo de la manera en
    que haya estructurado la representaci�n intermedia de los
    programas.

    Se declara derivando de @Show@ para que puedan probar la
    M�quina Logo individualmente y pueda evaluarse la creaci�n
    de fragmentos de c�digo "manualmente".

    Se declara derivando de @Eq@ pues en la implantaci�n interna
    de la m�quina es necesario comparar instrucciones.
 -}
data LogoProgram = Fd Int                -- ^ Avanzar N pasos.
                 | Bk Int                -- ^ Retroceder N pasos.
                 | Rt Int                -- ^ Girar N grados a la derecha.
                 | Lt Int                -- ^ Girar N grados a la izquierda.
                 | Pu                    -- ^ Subir el l�piz.
                 | Pd                    -- ^ Bajar el l�piz.
                 | Pc String             -- ^ Cambiar el color del l�piz.
                 | Say String            -- ^ Emitir un mensaje de texto.
                 | Home                  -- ^ Regresar al origen.
                 | Seq [LogoProgram]     -- ^ Secuenciaci�n de instrucciones.
                 | Rep Int [LogoProgram] -- ^ Repetir N veces.
                 deriving (Show, Eq)

{-
   Catamorfismo sobre el Tipo de Datos de las instrucciones de la
   M�quina Logo.
 -}
cataLogo a b c d e f g h i j k (Fd n)    = a n
cataLogo a b c d e f g h i j k (Bk n)    = b n
cataLogo a b c d e f g h i j k (Rt n)    = c n
cataLogo a b c d e f g h i j k (Lt n)    = d n
cataLogo a b c d e f g h i j k Pu        = e
cataLogo a b c d e f g h i j k Pd        = f
cataLogo a b c d e f g h i j k (Pc s)    = g s
cataLogo a b c d e f g h i j k (Say s)   = h s
cataLogo a b c d e f g h i j k Home      = i
cataLogo a b c d e f g h i j k (Seq l)   = j (map (cataLogo a b c d e f g h i j k) l)
cataLogo a b c d e f g h i j k (Rep n l) = k n (map (cataLogo a b c d e f g h i j k) l)

{-
   validColors es una funci�n constante que produce una tabla con los
   colores v�lidos de la M�quina Logo, utilizando cadenas alfanum�ricas
   como clave de b�squeda.
 -}
validColors :: [ (String,Color) ]
validColors = [
                ( "black", Black ),
                ( "red", Red ), ( "green", Green ),     ( "blue", Blue ),
                ( "cyan", Cyan ), ( "magenta", Magenta ), ( "yellow", Yellow ),
                ( "white", White )
              ]

{-
   toColor es utilizada para convertir una cadena que especifica un
   color en el tipo de datos Color necesario para dibujar. En caso
   que la cadena a buscar no corresponda a un color definido por la
   M�quina Logo, la ejecuci�n aborta con un error.
 -} 
toColor :: String -> Color
toColor s =
  case f of
       Just c  -> c
       Nothing -> error $ "'" ++ s ++ "' es un color invalido"
  where f = lookup (map toLower s) validColors

{-
   Modelo de Estado de la M�quina Logo.

   Se utiliza un State Monad simple (en el cual no nos interesa el
   valor producto intermedio) para modelar el cambio de estados en
   la m�quina.

   El tipo de datos LogoStep sobre estados (s) y valores (a) se
   declara como una instancia de Monad con las definiciones tradicionales
   de un State Monad.
 -}
data LogoStep s a = ST (s -> (a,s))

instance Monad (LogoStep s) where
  return a     = ST (\x -> (a,x))
  (ST f) >>= g =
    ST (\s -> let (a,s') = f s
                  ST fun = g a
              in  fun s')

{-
   El estado de la M�quina Logo es una tupla. El primer componente
   de la tupla representa el estado de la tortuga y el segundo
   componente contiene el dibujo que ha ido trazando.

   El estado de la tortuga se representa como:

   1. La posici�n en que se encuentra la tortuga, que es un punto
      en coordenadas cartesianas enteras. Puesto que usamos estos
      puntos para graficar posteriormente, resulta conveniente
      utilizar el tipo Point proveniente de HGL.
   2. La direcci�n en la cual apunta la tortuga, indicada con un
      n�mero entero en grados m�dulo 360. Se define el tipo
      de datos Direction como un sin�nimo de Int para este prop�sito.
   3. El estado del l�piz, que puede ser arriba o abajo. Se define
      un tipo abstracto PenStatus para este prop�sito.
   4. El color del l�piz actualmente en uso (est� arriba o abajo).
      Puesto que usamos este atributo para graficar posteriormente,
      resulta conveniente utilizar el tipo Color proveniente de HGL.

   El dibujo que se ha ido trazando se representa como una lista
   de figuras poligonales. Al terminar la ejecuci�n del programa,
   la lista contendr� exclusivamente una lista de figuras, pero
   durante la ejecuci�n puede ocurrir el objeto Empty para indicar
   una l�nea reci�n iniciada pero a�n no completada. Cada l�nea
   poligonal viene acompa�ada del color con el cual debe ser
   dibujada.

 -}
type Direction   = Int
data PenStatus   = Up
                 | Down
                 deriving (Show,Eq)
type PenColor    = Color

data Figure = Poly PenColor [Point] 
            | Text PenColor Point String
            | Empty
            deriving (Show,Eq)

type TurtleState = (Point, Direction, PenStatus, PenColor)
type LogoState   = (TurtleState,[Figure])
type TurtleRun   = LogoStep LogoState ()

getState   (ST p) s = snd (p s)
getDrawing (ST p) s = snd (snd (p s))

stop :: TurtleRun
stop = return ()

pd :: TurtleRun
pd = ST penDown

penDown ((p, d, Up, c), f) = ( (), ( (p, d, Down, c), (Empty:f) ) )
penDown x                  = ( (), x )

pu :: TurtleRun
pu = ST penUp

penUp ( (p, d, Down, c), (Empty:f) ) = ( (), ( ( p, d, Up, c), f ) )
penUp ( (p, d, Down, c),        f  ) = ( (), ( ( p, d, Up, c), f ) )
penUp x                              = ( (), x )

pc :: String -> TurtleRun
pc s = ST (changeColor s)

changeColor s ( (p,d,ps,_), f ) = ( (), ( (p,d,ps, (toColor s)), f ) )

say :: String -> TurtleRun
say s = ST (sayMessage s)

sayMessage s ( (p,d,Up,  c), f )         = ( (), ( (p,d,Up,c), f ) )
sayMessage s ( (p,d,Down,c), (Empty:f) ) = ( (), ( (p,d,Down,c), ((Text c p s):f) ) )
sayMessage s ( (p,d,Down,c), f ) = ( (), ( (p,d,Down,c), ((Text c p s):f) ) )

fd :: Int -> TurtleRun
fd n = ST (moveForward n)

moveForward n ( (p,d,  Up,c),              f ) = 
  ( (), ( ((move p n d),d,Up,c),                   f ) )
moveForward n ( (p,d,Down,c), ((Poly cc l):f) ) =
  if (c == cc)
  then ( (), ( (np        ,d,Down,c),  (Poly cc (np:l):f)            ) )
  else ( (), ( (np        ,d,Down,c),  ((Poly  c [np,p]):(Poly cc l):f) ) )
  where
    np = move p n d
moveForward n ( (p,d,Down,c),    (Empty:f) ) =
  ( (), ( (np,        d,Down,c), ((Poly c [np,p]):f) ) )
  where
    np = move p n d
moveForward n ( (p,d,Down,c),            f ) =
  ( (), ( (np,        d,Down,c), ((Poly c [np,p]):f) ) )
  where
    np = move p n d

move (x,y) n d =
  let direc  = (pi * (fromIntegral d)) / 180
      nn     = fromIntegral n
      nx     = x + (round (nn * (cos direc)))
      ny     = y + (round (nn * (sin direc)))
  in (nx,ny)

bk :: Int -> TurtleRun
bk = fd . negate

lt :: Int -> TurtleRun
lt n = ST (leftTurn n)

leftTurn n turtle = ( (), left n turtle )

left n (((x,y),d,ps,pc),f) =
  let newdir = (d + n) `mod` 360
  in  (((x,y),newdir,ps,pc),f)

rt :: Int -> TurtleRun
rt = lt . negate

repN :: Int -> TurtleRun -> TurtleRun
repN 0 p = stop
repN n p = p >> (repN (n-1) p)

initial     = ( ((0,0), 90, Up, White), [] )

home :: TurtleRun
home = ST goHome

goHome ( (_,d,ps,pc), f ) = ( (), (((0,0),90,ps,pc), f) )

origin w h = (half w, half h)
             where
               half i = round ((fromIntegral i)/2)

monadicPlot = cataLogo fd bk rt lt pu pd pc say home seq rep
  where seq [] = stop
        seq l  = foldl1 (>>) l
        rep n s = repN n (seq s)

{-|
    La funci�n @runLogoProgram@ ejecuta un programa escrito con
    las instrucciones de la M�quina Logo, produciendo la salida
    gr�fica indicada.

    El programa abrir� una ventana con las dimensiones indicadas y
    el t�tulo suministrado como argumento. Se posicionar� la tortuga
    en el centro de la ventana correspondiente a la coordenada (0,0),
    apuntando hacia el tope de la pantalla (90 grados), con el l�piz
    blanco levantado.

    El programa se ejecutar�, produciendo los efectos gr�ficos
    especificados en �l, y al finalizar ser� necesario oprimir
    una tecla para terminar la ejecuci�n cerrando la ventana.
 -}
runLogoProgram :: Int            -- ^ Anchura en pixels de la ventana.
                  -> Int         -- ^ Altura en pixels de la ventana.
                  -> String      -- ^ T�tulo para la ventana.
                  -> LogoProgram -- ^ Instrucciones de la M�quina Logo.
                  -> IO ()
runLogoProgram w h t p = 
    runGraphics (
    do
      window <- openWindow t (w,h)
      drawInWindow window $ overGraphics (
        let f (Poly c p)   = withColor c $ polyline (map fix p)
            f (Text c p s) = withColor c $ text (fix p) s
            x0             = fst (origin w h)
            y0             = snd (origin w h)
            fix (x,y)      = (x0 + x, y0 - y)
        in map f (getDrawing (monadicPlot p) initial)
        )
      getKey window
      closeWindow window
    )

