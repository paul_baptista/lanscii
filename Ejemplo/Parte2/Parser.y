{
{-|
  /Analizador Sint�ctico para MiniLogo/


	Traductores e Interpretadores CI3725

	Versi�n 0.52 2007-05-06


	Group 0

	86-17791 Ernesto Hern�ndez-Novich


	Este m�dulo, desarrollado en Happy, implanta un Analizador
	Sint�ctico para MiniLogo.
	Sirve como ejemplo de uso tanto para Happy como para Haddock e
	ilustra los conceptos de tipos recursivos en Haskell necesarios
	para la Parte Dos del Proyecto.

-}
module Parser (
	-- * Funciones exportadas.
	-- ** Analizador Sint�ctico con An�lisis de Contexto Est�tico.
	parser
) where

import Lexer
import SymTable

}

%name parser
%error { syntaxError }
%tokentype { Token }
%token
  program  { TkProgram   _    }
  begin    { TkBegin     _    }
	end      { TkEnd       _    }
	variable { TkVariable  _    }
	'['      { TkLB        _    }
	']'      { TkRB        _    }
	','      { TkComma     _    }
	to       { TkTo        _    }
	':='     { TkAssign    _    }
	forward  { TkForward   _    }
	right    { TkRight     _    }
	left     { TkLeft      _    }
  pendown  { TkPD        _    }
  penup    { TkPU        _    }
  pencolor { TkPC        _    }
  ';'      { TkSemicolon _    }
  if       { TkIf        _    }
	else     { TkElse      _    }
	repeat   { TkRepeat    _    }
  while    { TkWhile     _    }
  call     { TkCall      _    }
  using    { TkUsing     _    }
  return   { TkReturn    _    }
  stop     { TkStop      _    }
  '+'      { TkSum       _    }
  '-'      { TkMinus     _    }
  '*'      { TkTimes     _    }
  '/'      { TkDiv       _    }
  '%'      { TkMod       _    }
  '('      { TkLP        _    }
  ')'      { TkRP        _    }
  '<'      { TkLT        _    }
  '<='     { TkLE        _    }
  '>'      { TkGT        _    }
  '>='     { TkGE        _    }
  '='      { TkEQ        _    }
  '!='     { TkNE        _    }
  num      { TkNum       _ $$ }
  id       { TkId        _ $$ }
%%

Prog : program Decls begin Main end { ($1,$2) }

Decls : Decls VarDecl
      | Decls ProcDecl
			|

VarDecl : variable '[' IdList ']'

ProcDecl : to id '[' ParmList ']' Proc end

IdList : id
       | IdList ',' id

ParmList : IdList
         |

SimpleStmt : id ':=' Expr
           | forward Expr
					 | right
					 | left
           | pendown
					 | penup
					 | pencolor id
					 | stop

ProcStmt : SimpleStmt
         | if Boolean [ ProcStmt ]
				 | if Boolean [ ProcStmt ] else [ ProcStmt ]
				 | repeat Expr [ ProcStmt ]
				 | while Boolean [ ProcStmt ]
				 | return
				 | stop

Proc : ProcStmt
     | Proc ';' ProcStmt

MainStmt : SimpleStmt
         | if Boolean [ MainStmt ]
				 | if Boolean [ MainStmt ] else [ MainStmt ]
				 | repeat Expr [ MainStmt ]
				 | while Boolean [ MainStmt ]
				 | call id using [ ExprList ]
				 | stop

Main : MainStmt
     | Main ';' MainStmt

Boolean : Expr '<' Expr
        | Expr '<=' Expr
				| Expr '>' Expr
				| Expr '>=' Expr
				| Expr '=' Expr
				| Expr '!=' Expr

Expr : Expr '+' Expr
     | Expr '-' Expr
		 | Expr '*' Expr
		 | Expr '/' Expr
		 | Expr '%' Expr
		 | '-' Expr
		 | '(' Expr ')'
		 | num
		 | id

ExprList :
         | Expr
				 | ExprList ',' Expr

{

{-
		La funci�n @insertVar@ es de uso interno en la acci�n
		sem�ntica asociada a las producciones que reconocen una
		lista de declaraciones de variables, para tomar el identificador
		'i' y la expresi�n 'e' que tendr� asociada e insertarlos en
		la Tabla de S�mbolos.

		Si la Tabla de S�mbolos ya contiene el identificador, eso
		constituye un Error Est�tico que se reporta inmediatamente,
		abortando la ejecuci�n del Analizador Sint�ctico.

		Si la Tabla de S�mbolos no contiene el identificador, se utiliza
		la primitiva de inserci�n sobre Tablas de S�mbolo, teniendo cuidado
		de "envolver" la expresi�n con el constructor Just del tipo
		Maybe Expr.

-}
insertVar :: String -> Expr -> SymTable -> SymTable
insertVar i e t  =
	if isMember i t
	then error ("Error: Variable " ++ i ++ " ha sido redefinido!")
	else insert i (Just e) t

{-
		La funci�n @syntaxError@ es de uso interno, y es necesaria para
		completar la definici�n del parser generado por Happy. Ser� invocada
		cuando el parser detecta un error de sintaxis y recibe como
		argumento la lista de Tokens que falta por procesar.

		Como m�nimo es �til para abortar el procesamiento con un error
		que indique el Token que produjo el error y algo de contexto para
		que el programador pueda corregir su programa.
-}
syntaxError :: [Token] -> a
syntaxError (t:ts) = error $ 
                       "Error de sintaxis en el Token " ++ (show t) ++ "\n" ++
                       "Seguido de: " ++ (unlines $ map show $ take 3 ts)

}

