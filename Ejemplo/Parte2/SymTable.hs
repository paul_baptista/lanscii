{-|
  /Tabla de S�mbolos para la Calculadora/


	Traductores e Interpretadores CI3725

	Versi�n 0.52 2007-05-06


	Group 0

	86-17791 Ernesto Hern�ndez-Novich


	Este m�dulo, desarrollado en Haskell, implanta una Tabla de
	S�mbolos a ser utilizado por el Analizador Sint�ctico y el
	Interpretador de la calculadora vista en clase.
	Ilustra los conceptos de tipos recursivos en Haskell necesarios
	para la Parte Dos del Proyecto, as� como la manera de separar
	los elementos de manipulaci�n del Arbol Abstracto, del proceso
	concreto de construcci�n.

-}
module SymTable (
	-- * Tipos exportados.
	-- ** Expresiones (sumas y restas).
	Expr (..),
	-- ** T�rminos (productos y divisiones).
	Term (..),
	-- ** Factores (n�meros, identificadores y par�ntesis).
	Factor (..),
	-- ** Tabla de S�mbolos.
	SymTable,
	-- ** Valores asociados a los S�mbolos de la Tabla.
	Symbol,
	-- * Funciones exportadas.
	-- *** Determinar membres�a de un S�mbolo en una Tabla de S�mbolos.
	isMember,
	-- *** Buscar el valor asociado a un S�mbolo en una Tabla de S�mbolos.
	find,
	-- *** Insertar el valor asociado a un S�mbolo en una Tabla de S�mbolos.
	insert,
	-- *** Reemplazar el valor asociado a un S�mbolo en una Tabla de S�mbolos.
	replace
) where

import Lexer

{-|
		El tipo de datos @Expr@ representa las expresiones
		de menor precedencia (sumas y restas) en el �rbol abstracto
		para expresiones aritm�ticas.

		Se declara derivando de @Show@ para que se pueda probar el
		Analizador Sint�ctico individualmente, puesto que al invocar
		la funci�n @parser@ la estructura producida ser� presentada
		directamente en pantalla.

		Se declara derivando de @Eq@ pues ser� necesario para hacer
		el An�lisis de Contexto y la Interpretaci�n.
-}
data Expr = Add Expr Term         -- ^ Suma de una Expresi�n y un T�rmino.
					| Substract Expr Term   -- ^ Resta de una Expresi�n y un T�rmino.
					| Term Term             -- ^ Expresi�n sin sumas ni restas.
					deriving (Eq,Show)

{-|
		El tipo de datos @Term@ representa las expresiones
		de precedencia intermedia (productos y divisiones) en el �rbol
		abstracto para expresiones aritm�ticas.

		Se declara derivando de @Show@ para que se pueda probar el
		Analizador Sint�ctico individualmente, puesto que al invocar
		la funci�n @parser@ la estructura producida ser� presentada
		directamente en pantalla.

		Se declara derivando de @Eq@ pues ser� necesario para hacer
		el An�lisis de Contexto y la Interpretaci�n.
-}
data Term = Times Term Factor    -- ^ Producto de una Expresi�n y un Factor.
					| Divide Term Factor   -- ^ Cociente de una Expresi�n y un Factor.
					| Factor Factor        -- ^ T�rmino sin productos ni cocientes.
					deriving (Eq,Show)

{-|
		El tipo de datos @Factor@ representa las expresiones
		de precedencia superior (variables, n�meros y par�ntesis) en
		el �rbol abstracto para expresiones aritm�ticas.

		Se declara derivando de @Show@ para que se pueda probar el
		Analizador Sint�ctico individualmente, puesto que al invocar
		la funci�n @parser@ la estructura producida ser� presentada
		directamente en pantalla.

		Se declara derivando de @Eq@ pues ser� necesario para hacer
		el An�lisis de Contexto y la Interpretaci�n.
-}
data Factor = Expr Expr    -- ^ Expresi�n entre par�ntesis.
						| Num Integer  -- ^ N�mero literal.
						| Var String   -- ^ Identificador.
						deriving (Eq,Show)

{-|
		El tipo de datos @Symbol@ es un /alias/ para el tipo de
		datos @Maybe Expr@. El tipo de datos @Maybe a@ viene inclu�do
		en Haskell y permite modelar dos valores posibles:

		* Nada, representado con @Nothing@.

		* Un valor del tipo @a@, representado por @Just a@.

		En nuestro caso nos interesa representar que un s�mbolo dentro
		de la tabla puede no tener ning�n valor (i.e. @Nothing@) o
		bien un valor que es una expresi�n (i.e. @Just Expr@).
-}
type Symbol   = Maybe Expr

{-|
		El tipo de datos @SymTable@ se utilizar� para modelar la Tabla
		de S�mbolos que genera el analizador sint�ctico a medida que
		encuentra definiciones. Es un /alias/ para una lista de tuplas,
		donde el primer elemento de la tupla es el s�mbolo particular
		representado con un @String@ y el valor asociado es del tipo
		@Symbol@ descrito anteriormente.

		Las operaciones sobre nuestras tablas de s�mbolo asumen que
		/no/ hay repeticiones en la lista, y que los elementos nuevos
		se agregan al principio. Recordemos que la /eficiencia/ de los
		accesorios no es nuestra preocupaci�n en �sta materia :-)
-}
type SymTable = [ (String,Symbol) ]

{-|
		La funci�n @isMember@ determina si un identificador particular
		est� contenido en una tabla de s�mbolos, retornando un valor
		booleano acorde.
-}
isMember :: String       -- ^ S�mbolo a buscar en la Tabla de S�mbolos.
						-> SymTable  -- ^ Tabla de S�mbolos.
						-> Bool      -- ^ �El S�mbolo est� en la Tabla de S�mbolos?
isMember s []    = False
isMember s (e:t) = if fst(e) == s then True else isMember s t

{-|
		La funci�n @find@ obtiene el valor asociado a un identificador
		particular en una Tabla de S�mbolos. Existen tres posibilidades:
		
		* Si el identificador no est� contenido en la Tabla de S�mbolos
		  se retorna @Nothing@.

		* Si el identificador est� contenido en la Tabla de S�mbolos
		  se retorna su valor asociado, que puede ser:
			
		** @Nothing@.

		** @Just Expr@.

		Note que esto deja al programador la responsabilidad de determinar
		si el identificador est� contenido en la Tabla de S�mbolos antes
		de buscar su valor asociado.
-}
find :: String       -- ^ S�mbolo a buscar en la Tabla de S�mbolos.
				-> SymTable  -- ^ Tabla de S�mbolos.
				-> Symbol    -- ^ Valor asociado al S�mbolo, si existe.
find i []    = Nothing
find i (e:t) = if fst(e) == i then snd(e) else find i t

{-|
		La funci�n @insert@ agrega un nuevo identificador con su
		valor asociado dentro de una Tabla de S�mbolos, retornando
		la Tabla de S�mbolos modificada.

		Si el identificador ya estaba en la Tabla de S�mbolos, esta
		�ltima no es modificada.
-}
insert :: String      -- ^ S�mbolo a insertar en la Tabla de S�mbolos.
					-> Symbol   -- ^ Valor a asociar.
					-> SymTable -- ^ Tabla de S�mbolos donde insertar.
					-> SymTable -- ^ Nueva Tabla de S�mbolos despu�s de la inserci�n.
insert i v t = if isMember i t then t else (i,v) : t

{-|
		La funci�n @replace@ reemplaza el valor asociado a un
		identificador con un nuevo valor dentro de una Tabla de S�mbolos,
		retornando la Tabla de S�mbolos modificada.

		Si el identificador no est� en la Tabla de S�mbolos, esta
		�ltima no es modificada.
-}
replace ::	String      -- ^ S�mbolo cuyo valor se quiere modificar.
						-> Symbol   -- ^ Nuevo valor asociado al s�mbolo.
						-> SymTable -- ^ Tabla de S�mbolos a modificar.
						-> SymTable -- ^ Tabla de S�mbolos despu�s de la modificaci�n.
replace i v t = 
	if isMember i t then doReplace i v t [] else t
	where
		doReplace i v (e:t) x = if fst(e) == i
											 		  then x ++ (i,v) : t 
													  else doReplace i v t (x ++ [e])
