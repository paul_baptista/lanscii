{
{-|
  /Analizador Lexicogr�fico para MiniLogo/


	Traductores e Interpretadores CI3725

	Versi�n 0.1 2007-05-17


  Grupo 0

	86-17791 Ernesto Hern�ndez-Novich <mailto:emhn@usb.ve>


	Este m�dulo, desarrollado en Alex, implanta un Analizador
	Lexicogr�fico para el lenguaje MiniLogo siguiendo la
	especificaci�n del mismo.

	Se implantaron algunas funciones auxiliares no exportadas,
	cuya documentaci�n se encuentra en el c�digo fuente.

	Si lo desea, puede utilizar este m�dulo para sustituir
	/enteramente/ su m�dulo Lexer, con la condici�n que no
	puede cambiarlo en modo alguno durante el resto del Proyecto.
-}
module Lexer (
  -- * Tipos exportados.
	-- ** /Tokens/ producidos por el Analizador Lexicogr�fico.
	Token(..),
	-- * Funciones exportadas.
	-- ** Analizador Lexicogr�fico.
	lexer 
) where
}

%wrapper "posn"

$digito = 0-9           -- UN digito
$letra  = [a-zA-Z]      -- UNA letra

tokens :-

	$white+                        ;
	\/\/.*                         ;
	program                        { \p s -> TkProgram  (lyc p)          }
	begin                          { \p s -> TkBegin    (lyc p)          }
	end                            { \p s -> TkEnd      (lyc p)          }
	variable                       { \p s -> TkVariable (lyc p)          }
	\[                             { \p s -> TkLB       (lyc p)          }
	\]                             { \p s -> TkRB       (lyc p)          }
	\,                             { \p s -> TkComma    (lyc p)          }
	to                             { \p s -> TkTo       (lyc p)          }
	:=                             { \p s -> TkAssign   (lyc p)          }
	forward                        { \p s -> TkForward  (lyc p)          }
	right                          { \p s -> TkRight    (lyc p)          }
	left                           { \p s -> TkLeft     (lyc p)          }
  pendown                        { \p s -> TkPD       (lyc p)          }
  penup                          { \p s -> TkPU       (lyc p)          }
  pencolor                       { \p s -> TkPC       (lyc p)          }
	\;                             { \p s -> TkSeq      (lyc p)          }
	if                             { \p s -> TkIf       (lyc p)          }
	else                           { \p s -> TkElse     (lyc p)          }
	repeat                         { \p s -> TkRepeat   (lyc p)          }
	while                          { \p s -> TkWhile    (lyc p)          }
	call                           { \p s -> TkCall     (lyc p)          }
	using                          { \p s -> TkUsing    (lyc p)          }
	return                         { \p s -> TkReturn   (lyc p)          }
	stop                           { \p s -> TkStop     (lyc p)          }
  \+                             { \p s -> TkSum      (lyc p)          }
  \-                             { \p s -> TkMinus    (lyc p)          }
  \*                             { \p s -> TkTimes    (lyc p)          }
  \/                             { \p s -> TkDiv      (lyc p)          }
  \%                             { \p s -> TkMod      (lyc p)          }
  \(                             { \p s -> TkLP       (lyc p)          }
  \)                             { \p s -> TkRP       (lyc p)          }
  \<                             { \p s -> TkLT       (lyc p)          }
  \<\=                           { \p s -> TkLE       (lyc p)          }
  \>                             { \p s -> TkGT       (lyc p)          }
  \>\=                           { \p s -> TkGE       (lyc p)          }
  \=                             { \p s -> TkEQ       (lyc p)          }
  \!\=                           { \p s -> TkNE       (lyc p)          }
	$digito+                       { \p s -> TkNum      (lyc p) (read s) }
  $letra [ $letra $digito _ ]*   { \p s -> TkId       (lyc p) s        }
{

{-|
    El tipo de datos @Token@ representa los diferentes /tokens/
		producidos por el Analizador Lexicogr�fico. Cada /token/
		va acompa�ado de una tupla de enteros, cuyos componentes
		denotan la l�nea y columna, respectivamente, en la cual se
		encontr� el /token/ dentro del archivo procesado. Para aquellos
		/tokens/ que lo ameriten, se agrega el lexema convertido al tipo
		deseado, i.e. si es un n�mero se convierte en @Int@ y si es
		una cadenas se convierte en @String@.

		El tipo de datos @Token@ se declara derivando de @Show@ para que
		se pueda probar el Analizador Lexicogr�fico individualmente, puesto
		que al invocar la funci�n @lexer@ la lista producida ser� presentada
		directamente en pantalla.

		El tipo de datos @Token@ Se declara derivando de @Eq@ pues es
		requerido para el funcionamiento adecuado del Analizador Sint�ctico
		(/parser/).
-}

data Token = TkProgram  (Int,Int)  -- ^ Palabra reservada @program@.
           | TkBegin    (Int,Int)  -- ^ Palabra reservada @begin@.
           | TkEnd      (Int,Int)  -- ^ Palabra reservada @end@.
           | TkVariable (Int,Int)  -- ^ Palabra reservada @variable@.
           | TkLB       (Int,Int)  -- ^ Corchete izquierdo @[@.
           | TkRB       (Int,Int)  -- ^ Corchete derecho @]@.
           | TkComma    (Int,Int)  -- ^ Coma @,@.
           | TkTo       (Int,Int)  -- ^ Palabra reservada @to@.
           | TkAssign   (Int,Int)  -- ^ Instrucci�n de asignaci�n @:=@.
           | TkForward  (Int,Int)  -- ^ Instrucci�n de desplazamiento @forward@.
           | TkRight    (Int,Int)  -- ^ Instrucci�n de rotaci�n @right@.
           | TkLeft     (Int,Int)  -- ^ Instrucci�n de rotaci�n @left@.
           | TkPD       (Int,Int)  -- ^ Instrucci�n de trazado @pendown@.
           | TkPU       (Int,Int)  -- ^ Instrucci�n de trazado @penup@.
           | TkPC       (Int,Int)  -- ^ Instrucci�n de trazado @pencolor@.
           | TkSeq      (Int,Int)  -- ^ Secuenciaci�n de instrucciones @;@.
           | TkIf       (Int,Int)  -- ^ Instrucci�n condicional @if@.
           | TkElse     (Int,Int)  -- ^ Instrucci�n condicional @else@.
           | TkRepeat   (Int,Int)  -- ^ Iteraci�n determinada @repeat@.
           | TkWhile    (Int,Int)  -- ^ Iteraci�n indeterminada @while@.
           | TkCall     (Int,Int)  -- ^ Invocaci�n de procedimiento @call@.
           | TkUsing    (Int,Int)  -- ^ Pasaje de par�metros @using@.
           | TkReturn   (Int,Int)  -- ^ Retornar desde procedimiento @return@.
           | TkStop     (Int,Int)  -- ^ Terminaci�n de programa @stop@.
           | TkSum      (Int,Int)  -- ^ Suma aritm�tica @+@.
           | TkMinus    (Int,Int)  -- ^ Signo @-@ (resta aritm�tica o unario).
           | TkTimes    (Int,Int)  -- ^ Producto aritm�tico @*@.
           | TkDiv      (Int,Int)  -- ^ Divisi�n aritm�tica @\/@.
           | TkMod      (Int,Int)  -- ^ Resto de la divisi�n aritm�tica @%@.
					 | TkLP       (Int,Int)  -- ^ Par�ntesis izquierdo @(@.
					 | TkRP       (Int,Int)  -- ^ Par�ntesis derecho @)@.
					 | TkLT       (Int,Int)  -- ^ Menor que @<@.
					 | TkLE       (Int,Int)  -- ^ Menor o igual que @<=@.
					 | TkGT       (Int,Int)  -- ^ Mayor que @>@.
					 | TkGE       (Int,Int)  -- ^ Mayor o igual que @>=@.
					 | TkEQ       (Int,Int)  -- ^ Igual que @=@.
					 | TkNE       (Int,Int)  -- ^ Diferente de @!=@.
					 | TkId       (Int,Int) String -- ^ Identificador alfanum�rico.
					 | TkNum      (Int,Int) Int    -- ^ N�mero entero literal.
           deriving (Eq, Show)

{-|
    La funci�n @lexer@ encapsula el uso del Analizador Lexicogr�fico.
		Recibe como �nico argumento un @String@, presumiblemente le�do
		desde un archivo pero tambi�n es posible pasarlo expl�citamente,
		y produce una lista de /tokens/ a medida que los va procesando.

		En la versi�n Extra de MiniLogo:

		* Si no hay errores, retorna la lista de /tokens/ reconocidos.

		* Si hay errores lexicogr�ficos reporta en pantalla /todos/ los
		  errores encontrados y el programa termina su ejecuci�n /sin/
			retornar /token/ alguno.

-}
lexer :: String      -- ^ Cadena de caracteres @S@ a "tokenizar"
         -> [Token]  -- ^ Lista resultante de /tokens/ del tipo @Token@.

lexer s = if null(errores)
          then
						tokens
					else
						error (concat errores)
					where
						(tokens,errores) = processInput s

{-
    La funci�n processInput es la que reemplaza a alexScanTokens para
		"hacer el trabajo".

		Recibe como argumento la cadena de entrada y la procesa utilizando
		una funci�n auxiliar 'go' (visible solamente dentro de la funci�n
		en virtud de la cl�usula 'where' m�s externa).

		La funci�n go trabaja con recursi�n de cola para producir una
		tupla de listas. El primer elemento de la tupla tiene todos los
		tokens reconocidos y el segundo elemento de la tupla tiene todos
		los errores encontrados.

		* La funci�n go es invocada utilizando la posici�n inicial del
		  archivo con la cadena completa, y una tupla con dos listas
			vac�as, indicando el estado inicial: no hemos reconocido ning�n
			token, no hemos encontrado ning�n error.
    * En cada paso de la funci�n go se utiliza alexScan sobre "lo que
		  queda" de la cadena de entrada y pueden ocurrir cuatro cosas:
			1. Se acab� el string (AlexEOF), en ese caso s�lo hay que
			   retornar la tupla con los tokens que llevamos y los errores
				 que encontramos.
			2. Encontr� un caracter que puede saltarse, en ese caso se
			   llama recursivamente a go con los mismos tokens y errores
				 que llevamos acumulados ("no hizo nada").
			3. Se encontr� un token v�lido, en ese caso hay que retornar
			   una tupla construida de la siguiente forma:
				 * Se ejecuta la acci�n sobre el segmento de cadena de
				   entrada que corresponde al lexema, y eso va a producir
					 el Token concreto que encontramos.
				 * Se invoca recursivamente a go con el resto de la
				   cadena de entrada, para obtener los siguientes tokens
					 y, si ocurren, m�s errores.
				 * Se construye la tupla a retornar colocando el token
				   encontrado al princpio y preservando la lista de
					 errores.
      4. Se encontr� un caracter inesperado produciendo un error,
			   en ese caso hay que retornar una tupla construida de la
				 siguiente forma:
				 * Se invoca recursivamente a go con el resto de la
				   cadena de entrada despu�s de saltarnos el caracter que
					 produjo el error, para obtener los siguientes tokens
					 y, si ocurren, m�s errores. Note que para que Alex
					 no "pierda la posici�n" tenemos que utilizar la
					 funci�n interna alexMove para convertir el AlexPosition
					 anterior en uno actualizado seg�n sea necesario; eso
					 lo hace la funci�n auxiliar local skipChar.
         * Se prepara un nuevo mensaje de error indicando la
				   posici�n actual y el caracter que produjo el error; eso
					 lo hace la funci�n auxiliar local addError.
 -}
processInput :: String -> ([Token],[String])
processInput str = go (alexStartPos,'\n',str) ([],[])
	where go inp@(pos,_,str) (ts,es) =
		case alexScan inp 0 of
			AlexEOF                -> (ts,es)
			AlexSkip  inp' len     -> go inp' (ts,es)
			AlexToken inp' len act -> (tsf,esf)
			  where
				  (ts',esf) = go inp' (ts,es)
				  tsf       = act pos (take len str) : ts'
			AlexError inp'         -> (tsf,esf)
				where
					(tsf,es')           = go (skipChar inp') (ts,es)
					esf                 = addError inp' es'
					skipChar (p,c,s)    = (alexMove p (head s),c,(tail s))
					addError (p,_,s) es = m : es
						where
							(l,c) = lyc p
							m     = "\nError (Lexer): Caracter inesperado '" ++ 
											[ head s ] ++
											"' en la linea " ++ (show l) ++
											" y columna " ++ (show c) ++ "."

{-
    Cada token debe ir acompa�ado de la l�nea y columna en la cual
		fue encontrado. El wrapper 'posn' genera para cada token el
		desplazamiento absoluto dentro del archivo, la linea y la columna.
		Como el enunciado del proyecto establece que solamente interesa
		la l�nea y la columna, la funci�n lyc ("linea y columna") extrae
		solamente la l�nea y la columna, produciendo una tupla susceptible
		de ser incorporada directamente al token.
 -}
lyc :: AlexPosn -> (Int,Int)
lyc (AlexPn _ l c) = (l,c)

}
