{
{-|
  /Analizador Lexicográfico para MiniLogo/


	Traductores e Interpretadores CI3725

	Versión 0.1 2007-05-17


  Grupo 0

	86-17791 Ernesto Hernández-Novich <mailto:emhn@usb.ve>


	Este módulo, desarrollado en Alex, implanta un Analizador
	Lexicográfico para el lenguaje MiniLogo siguiendo la
	especificación del mismo.

	Se implantaron algunas funciones auxiliares no exportadas,
	cuya documentación se encuentra en el código fuente.
-}
module Lexer (
  -- * Tipos exportados.
	-- ** /Tokens/ producidos por el Analizador Lexicográfico.
	Token(..),
	-- * Funciones exportadas.
	-- ** Analizador Lexicográfico.
	lexer 
) where
}

%wrapper "posn"

$digito = 0-9           -- UN digito
$letra  = [a-zA-Z]      -- UNA letra

tokens :-

	$white+                        ;
	\/\/.*                         ;
	program                        { \p s -> TkProgram  (lyc p)          }
	begin                          { \p s -> TkBegin    (lyc p)          }
	end                            { \p s -> TkEnd      (lyc p)          }
	variable                       { \p s -> TkVariable (lyc p)          }
	\[                             { \p s -> TkLB       (lyc p)          }
	\]                             { \p s -> TkRB       (lyc p)          }
	\,                             { \p s -> TkComma    (lyc p)          }
	to                             { \p s -> TkTo       (lyc p)          }
	:=                             { \p s -> TkAssign   (lyc p)          }
	forward                        { \p s -> TkForward  (lyc p)          }
	right                          { \p s -> TkRight    (lyc p)          }
	left                           { \p s -> TkLeft     (lyc p)          }
  pendown                        { \p s -> TkPD       (lyc p)          }
  penup                          { \p s -> TkPU       (lyc p)          }
  pencolor                       { \p s -> TkPC       (lyc p)          }
	\;                             { \p s -> TkSeq      (lyc p)          }
	if                             { \p s -> TkIf       (lyc p)          }
	else                           { \p s -> TkElse     (lyc p)          }
	repeat                         { \p s -> TkRepeat   (lyc p)          }
	while                          { \p s -> TkWhile    (lyc p)          }
	call                           { \p s -> TkCall     (lyc p)          }
	using                          { \p s -> TkUsing    (lyc p)          }
	return                         { \p s -> TkReturn   (lyc p)          }
	stop                           { \p s -> TkStop     (lyc p)          }
  \+                             { \p s -> TkSum      (lyc p)          }
  \-                             { \p s -> TkMinus    (lyc p)          }
  \*                             { \p s -> TkTimes    (lyc p)          }
  \/                             { \p s -> TkDiv      (lyc p)          }
  \%                             { \p s -> TkMod      (lyc p)          }
  \(                             { \p s -> TkLP       (lyc p)          }
  \)                             { \p s -> TkRP       (lyc p)          }
  \<                             { \p s -> TkLT       (lyc p)          }
  \<\=                           { \p s -> TkLE       (lyc p)          }
  \>                             { \p s -> TkGT       (lyc p)          }
  \>\=                           { \p s -> TkGE       (lyc p)          }
  \=                             { \p s -> TkEQ       (lyc p)          }
  \!\=                           { \p s -> TkNE       (lyc p)          }
	$digito+                       { \p s -> TkNum      (lyc p) (read s) }
  $letra [ $letra $digito _ ]*   { \p s -> TkId       (lyc p) s        }
	.                              { reportError }
{

{-|
    El tipo de datos @Token@ representa los diferentes /tokens/
		producidos por el Analizador Lexicográfico. Cada /token/
		va acompañado de una tupla de enteros, cuyos componentes
		denotan la línea y columna, respectivamente, en la cual se
		encontró el /token/ dentro del archivo procesado. Para aquellos
		/tokens/ que lo ameriten, se agrega el lexema convertido al tipo
		deseado, i.e. si es un número se convierte en @Int@ y si es
		una cadenas se convierte en @String@.

		El tipo de datos @Token@ se declara derivando de @Show@ para que
		se pueda probar el Analizador Lexicográfico individualmente, puesto
		que al invocar la función @lexer@ la lista producida será presentada
		directamente en pantalla.

		El tipo de datos @Token@ Se declara derivando de @Eq@ pues es
		requerido para el funcionamiento adecuado del Analizador Sintáctico
		(/parser/).
-}

data Token = TkProgram  (Int,Int)  -- ^ Palabra reservada @program@.
           | TkBegin    (Int,Int)  -- ^ Palabra reservada @begin@.
           | TkEnd      (Int,Int)  -- ^ Palabra reservada @end@.
           | TkVariable (Int,Int)  -- ^ Palabra reservada @variable@.
           | TkLB       (Int,Int)  -- ^ Corchete izquierdo @[@.
           | TkRB       (Int,Int)  -- ^ Corchete derecho @]@.
           | TkComma    (Int,Int)  -- ^ Coma @,@.
           | TkTo       (Int,Int)  -- ^ Palabra reservada @to@.
           | TkAssign   (Int,Int)  -- ^ Instrucción de asignación @:=@.
           | TkForward  (Int,Int)  -- ^ Instrucción de desplazamiento @forward@.
           | TkRight    (Int,Int)  -- ^ Instrucción de rotación @right@.
           | TkLeft     (Int,Int)  -- ^ Instrucción de rotación @left@.
           | TkPD       (Int,Int)  -- ^ Instrucción de trazado @pendown@.
           | TkPU       (Int,Int)  -- ^ Instrucción de trazado @penup@.
           | TkPC       (Int,Int)  -- ^ Instrucción de trazado @pencolor@.
           | TkSeq      (Int,Int)  -- ^ Secuenciación de instrucciones @;@.
           | TkIf       (Int,Int)  -- ^ Instrucción condicional @if@.
           | TkElse     (Int,Int)  -- ^ Instrucción condicional @else@.
           | TkRepeat   (Int,Int)  -- ^ Iteración determinada @repeat@.
           | TkWhile    (Int,Int)  -- ^ Iteración indeterminada @while@.
           | TkCall     (Int,Int)  -- ^ Invocación de procedimiento @call@.
           | TkUsing    (Int,Int)  -- ^ Pasaje de parámetros @using@.
           | TkReturn   (Int,Int)  -- ^ Retornar desde procedimiento @return@.
           | TkStop     (Int,Int)  -- ^ Terminación de programa @stop@.
           | TkSum      (Int,Int)  -- ^ Suma aritmética @+@.
           | TkMinus    (Int,Int)  -- ^ Signo @-@ (resta aritmética o unario).
           | TkTimes    (Int,Int)  -- ^ Producto aritmético @*@.
           | TkDiv      (Int,Int)  -- ^ División aritmética @\/@.
           | TkMod      (Int,Int)  -- ^ Resto de la división aritmética @%@.
					 | TkLP       (Int,Int)  -- ^ Paréntesis izquierdo @(@.
					 | TkRP       (Int,Int)  -- ^ Paréntesis derecho @)@.
					 | TkLT       (Int,Int)  -- ^ Menor que @<@.
					 | TkLE       (Int,Int)  -- ^ Menor o igual que @<=@.
					 | TkGT       (Int,Int)  -- ^ Mayor que @>@.
					 | TkGE       (Int,Int)  -- ^ Mayor o igual que @>=@.
					 | TkEQ       (Int,Int)  -- ^ Igual que @=@.
					 | TkNE       (Int,Int)  -- ^ Diferente de @!=@.
					 | TkId       (Int,Int) String -- ^ Identificador alfanumérico.
					 | TkNum      (Int,Int) Int    -- ^ Número entero literal.
           deriving (Eq, Show)

{-|
    La función @lexer@ encapsula el uso del Analizador Lexicográfico.
		Recibe como único argumento un @String@, presumiblemente leído
		desde un archivo pero también es posible pasarlo explícitamente,
		y produce una lista de /tokens/ a medida que los va procesando.

		En la versión Simple de MiniLogo, todo lo que hace es apoyarse
		en la función @alexScanTokens@ generada por Alex.
-}
lexer :: String      -- ^ Cadena de caracteres @S@ a "tokenizar"
         -> [Token]  -- ^ Lista resultante de /tokens/ del tipo @Token@.

lexer s = alexScanTokens s

{-
    Cada token debe ir acompañado de la línea y columna en la cual
		fue encontrado. El wrapper 'posn' genera para cada token el
		desplazamiento absoluto dentro del archivo, la linea y la columna.
		Como el enunciado del proyecto establece que solamente interesa
		la línea y la columna, la función lyc ("linea y columna") extrae
		solamente la línea y la columna, produciendo una tupla susceptible
		de ser incorporada directamente al token.
 -}
lyc :: AlexPosn -> (Int,Int)
lyc (AlexPn _ l c) = (l,c)

{-
    Cuando ninguna de las expresiones regulares para palabras reservadas,
		símbolos, números o identificadores logra coincidir con la entrada,
		quiere decir que se ha encontrado un caracter inválido para MiniLogo.
		La última expresión regular de la lista captura ese caracter inválido
		y debe emitir un mensaje de error "razonable".

		Cuando se usa el wrapper 'posn' todas las acciones son invocadas
		pasando como parámetros tanto la posición generada por Alex como
		la cadena (en este caso de exactamente un caracter) que coincidió
		con la expresión regular. La función reportError se encarga de emitir
		un mensaje de error apropiado aprovechando esos parámetros.
 -}
reportError :: AlexPosn -> String -> a
reportError p s = error m
	where 
		(l,c) = lyc p
		m     = "\nError (Lexer): Caracter inesperado '" ++ s ++ 
						"' en la linea " ++ (show l) ++ " y columna " ++ (show c) ++ "."
															
}
