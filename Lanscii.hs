-----------------------------------------------------------------------------
-- |
-- Modulo       :   Main
--
-- Autores      :   Daniel Leones (09-10977) <mailto:metaleones@gmail.com>,
--                  Paul Baptista (10-10056) <mailto:paul.baptista@gmail.com>
--
-- Grupo        :   16
--
-- Licencia     :   Apache License 2.0
--
-- Modulo principal. Se encarga de ejecutar un an�lisis sint�ctico a un
-- programa escrito en Lanscii y devolver su �rbol sint�ctico abstracto si es
-- correcto. Recibe como par�metro, o por entrada est�ndar, el nombre del
-- archivo a procesar e imprime el AST por la salida est�ndar.
-- 
-- Universidad Sim�n Bol�var
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------
module Main (
  main
) where

import System.Environment
import System.IO
import System.Exit
import Lexer
import Parser

{-|
   Funci�n principal.

   El programa puede ser compilado para su ejecuci�n directa,
   o bien cargado en el interpretador GHCi e invocado a trav�s
   de la funci�n @main@.
 -}
main :: IO ()
main =
  do
    args <- getArgs
    fileName <- getFilename args
    hSetBuffering stdout NoBuffering
    content <- readFile fileName
    print $ parser $ lexer content

{-
   getFilename

	 Funci�n auxiliar para obtener el nombre del archivo a procesar.
	 Presenta un prompt en pantalla para que el usuario introduzca
	 el nombre del archivo que desea procesar y espera a que se
	 suministre una l�nea de texto. Esa l�nea de texto es retornada.
-}
getFilename :: [String] -> IO String
getFilename args =
  if (null args) then
    do
      hSetBuffering stdout NoBuffering
      putStr "Archivo a Interpretar: "
      fileName <- getLine
      return fileName
  else
    do
      return (head args)